import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import ShallowRenderer from 'react-test-renderer/shallow';
import Address from '../Address';

describe('src/components/forms/Address', () => {
  useSelector.mockImplementation(fn => {
    fn({ form: { address: {} } });
    return ({ cities: [], provinces: [], subdistricts: [], villages: [], fetchArea: jest.fn() });
  });
  test('render',() => {
    const shallow = new ShallowRenderer();
    const tree = shallow.render(<Address/>);
    Address.defaultProps.handleSubmit();
    Address.defaultProps.onClose();
    Address.defaultProps.onPreview();
    Address.defaultProps.initialize();
    expect(tree).toMatchSnapshot();
  });

  test('simulate onPreview', () => {
    const onPreview = jest.fn();
    useSelector.mockImplementationOnce(() => ({ cities: [{ id: 1 }], fetchArea: jest.fn(),
      provinces: [{ id: 1 }], subdistricts: [{ id: 1 }], villages: [{ id: 1, name: 'test' }]}))
      .mockImplementationOnce(() => ({ store_village_id: '1' }));
    useEffect.mockImplementationOnce(fn => fn())
      .mockImplementationOnce(fn => fn())
      .mockImplementationOnce(fn => fn())
      .mockImplementationOnce(fn => fn())
      .mockImplementationOnce(fn => fn());
    Address({ ...Address.defaultProps, onPreview });
    expect(onPreview).toHaveBeenCalled;
  });
});
