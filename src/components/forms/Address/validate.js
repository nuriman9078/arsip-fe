export default function validate(values) {
  const { store_address, store_province_id, store_city_id,
    store_subdistrict_id, store_village_id } = values;
  const required = 'Harus diisi!';
  const errorMessage = (b, v, m) => {
    if (!b) {
      return m;
    }
    if (!v) {
      return required;
    }
    return '';
  };

  return {
    store_address: !store_address ? required : '',
    store_province_id: !store_province_id ? required : '',
    store_city_id: errorMessage(store_province_id, store_city_id, 'Isi provinsi terlebih dahulu'),
    store_subdistrict_id: errorMessage(store_city_id, store_subdistrict_id, 'Isi kabupaten / kota terlebih dahulu'),
    store_village_id: errorMessage(store_subdistrict_id, store_village_id, 'Isi kecamatan terlebih dahulu'),
  };
}
