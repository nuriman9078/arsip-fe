import React from 'react';
import { useHistory} from 'react-router-dom';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import Button from '../../elements/Button';
import Text from '../../fields/Text';
import Password from '../../fields/Password';
import styles from './styles.scoped.css';
import { useSelector } from 'react-redux';

export default function Login(props) {
  const history = useHistory();
  const { handleSubmit } = props;
  const { message,isLoading } = useSelector(s => s.login);
  const textInputProps = { placeholder: 'cth. admin@email.com' };
  const passwordInputProps = { placeholder: 'Min. 6 karakter' };

  return (
    <form className={styles.root} onSubmit={handleSubmit}>
      <Field component={Text} inputProps={textInputProps} label="Email" name="email"/>
      <Field component={Password} inputProps={passwordInputProps} label="Kata Sandi" name="password" />
      {message && <p className={styles.error}>{message}</p>}
      <Button disabled={isLoading} isLoading={isLoading} onSubmit={handleSubmit}>MASUK</Button>
      <p>
        Belum punya akun?
        <Button to="/register" className={styles.button} >Daftar</Button>
      </p>
    </form>
  );
}

Login.defaultProps = {
  handleSubmit: () => {},
  invalid: true
};

Login.propTypes = {
  handleSubmit: PropTypes.func,
  invalid: PropTypes.bool
};
