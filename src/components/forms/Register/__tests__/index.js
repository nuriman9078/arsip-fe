import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import ShallowRenderer from 'react-test-renderer/shallow';
import Register, { Document } from '../Register';

const defaultState = {
  isLoading: false,
};

useSelector.mockImplementation(fn => {
  fn({ form: { register: {} } });
  return defaultState;
});

describe('src/components/forms/Register', () => {
  test('render', () => {
    const shallow = new ShallowRenderer();
    const tree = shallow.render(<Register />);
    Register.defaultProps.handleSubmit();
    expect(tree).toMatchSnapshot();
  });

  test('show password', () => {
    const show = { confirmPassword: true, password: true };
    const setShow = jest.fn();

    useState.mockImplementationOnce(() => [show, setShow]);
    const result = Register(Register.defaultProps);
    result.props.children[5].props.onClickIcon();
    expect(setShow).toHaveBeenCalledWith({ ...show, password: false });

    result.props.children[6].props.onClickIcon();
    expect(setShow).toHaveBeenCalledWith({ ...show, confirmPassword: false });
  });

  // test('invalid', () => {
  //   useEffect.mockImplementationOnce(fn => fn()());

  //   const invalid = false;
  //   useSelector.mockImplementationOnce(() => ({ ...defaultState,
  //    address: { village_id: '123' } }));
  //   const result = Register({ ...Register.defaultProps, invalid });
  //   expect(result.props.children[16].props.disabled).toBe(false);
  // });

  test('Document', () => {
    const shallow = new ShallowRenderer();
    const tree = shallow.render(<Document />);
    expect(tree).toMatchSnapshot();
  });

  // test('upload document', () => {
  //   const dispatch = jest.fn();

  //   useDispatch.mockImplementationOnce(() => dispatch);
  //   const result = Document();
  //   result.props.children[1][0].props.uploadDocument();
  //   expect(dispatch).toHaveBeenCalled();
  // });
});
