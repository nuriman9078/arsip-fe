import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
// import { useSelector } from 'react-redux';
import { Field } from 'redux-form';
import Button from '../../../components/elements/Button';
// import Modal from '../../../components/elements/Modal';
import TextField from '../../../components/fields/Text';
import TextAreaField from '../../../components/fields/TextArea';
// import FileField from '../../../components/fields/File';
// import { getUserData } from '../../../utils/storage';
import AddressForm from '../Address';
import styles from './styles.scoped.css';

export default function Profile(props) {
  const { handleSubmit, invalid, change, touch } = props;
  const history = useHistory();

  useEffect(() => {
    touch('full_address', 'name', 'position', 'bank_name', 'logo_path',
      'bank_account_number', 'mobile_number');
  },[]);

  const textProps = [
    { label: 'Nama Perusahaan', placeholder: 'Isikan nama perusahaan', disabled: true, required: true },
    { label: 'Nama Person In Charge', placeholder: 'Isikan nama Anda', required: true },
    { label: 'Jabatan', placeholder: 'Isikan nama jabatan Anda', required: true },
    { label: 'Deskripsi Perusahaan', placeholder: 'Isikan deskripsi perusahaan' },
    { disabled: true, label: 'Alamat Email', required: true },
    { label: 'Nomor Telepon', placeholder: 'Isikan nomor telepon perusahaan', type: 'tel', required: true },
    { label: 'Nama Bank', placeholder: 'Isikan nama bank', required: true },
    { label: 'Nomor Rekening', placeholder: 'Isikan nomor rekening perusahaan', required: true },
  ];

  return (
    <form className={styles.root} onSubmit={handleSubmit}>
      <figure>
        <img alt="broken-image" src={"/assets/img-empty.svg"} />
        
      </figure>
      <section>
        <Field component={TextField} inputProps={textProps[0]} name="store_name"/>
        <section>
          <Field component={TextField} inputProps={textProps[1]} name="name" />
          <Field component={TextField} inputProps={textProps[2]} name="position" />
        </section>
        <Field component={TextAreaField} name="store_description" textAreaProps={textProps[3]} />
        <Address change={change} />
        <Field component={TextField} inputProps={textProps[4]} name="email" />
        <Field component={TextField} inputProps={textProps[5]} name="mobile_number" />
      </section>
      <footer>
        <Button variant="ghost">Batal</Button>
        <Button type="submit">Simpan</Button>
      </footer>
    </form>
  );
}

Profile.defaultProps = {
  change: () => { },
  handleSubmit: () => { },
  invalid: true,
  touch: () => { },
};

Profile.propTypes = {
  change: PropTypes.func,
  handleSubmit: PropTypes.func,
  invalid: PropTypes.bool,
  touch: PropTypes.func,
};


export function Address({ change }) {
  const { store_address, store_province_id, store_city_id, store_subdistrict_id,
    store_village_id } = formData || {};
  const [open, setOpen] = useState(false);
  const onClose = () => setOpen(false);
  const onOpen = () => setOpen(true);
  const submit = (v) => {
    Object.keys(v).map(key => change(key, v[key]));
    onClose();
  };
  const initial = { store_address, store_province_id, store_city_id,
    store_subdistrict_id, store_village_id };
  const inputProps = { label: 'Alamat Perusahaan', required: true, readOnly: 'readonly' };
  return(
    <div className={styles.address}>
      {store_village_id?
        <>
          <Field component={TextField} inputProps={inputProps} name="full_address"/>
          <Button className={styles.edit} onClick={onOpen} size="small">Edit</Button>
        </> :
        <>
          <p>Alamat Perusahaan<span>*</span></p>
          <Button fixed onClick={onOpen} size="large">Isi Alamat Perusahaan Anda disini</Button>
        </>
      }
      <Modal className={styles.modal} onClose={onClose} open={open}>
        <h4>Isi Alamat Perusahaan</h4>
        <AddressForm formName="profile" initial={initial} onClose={onClose}
          onPreview={(v) => change('full_address', v)} onSubmit={submit}/>
      </Modal>
    </div>);
}

Address.defaultProps = {
  change: () => { },
};

Address.propTypes = {
  change: PropTypes.func,
};
