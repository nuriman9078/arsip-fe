import validate from '../validate';

jest.mock('../../../../utils/validation', () => ({
  isMobilePhone: v => v === '081',
}));

describe('src/components/forms/Profile/validate', () => {
  test('validate', () => {
    const input1 = {
      store_name: '',
      name: '',
      bank_name: '',
      bank_account_number: '',
      position: '',
      logo_path: '',
      mobile_number: ''
    };
    expect(validate(input1)).toMatchObject({
      store_name: 'Harus diisi!',
      name: 'Harus diisi!',
      bank_name: 'Harus diisi!',
      bank_account_number: 'Harus diisi!',
      position: 'Harus diisi!',
      logo_path: 'Harus diisi!',
      mobile_number: 'Mohon memasukkan no. handphone yang benar'
    });

    const input2 = {
      store_name: 'test',
      name: 'test',
      bank_name: 'test',
      bank_account_number: '0812',
      position: 'ceo',
      logo_path: 'image',
      mobile_number: '081'
    };
    expect(validate(input2)).toMatchObject({
      store_name: '',
      name: '',
      bank_name: '',
      bank_account_number: '',
      position: '',
      logo_path: '',
      mobile_number: ''
    });
  });
});
