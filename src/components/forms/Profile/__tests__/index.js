import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import ShallowRenderer from 'react-test-renderer/shallow';
import Profile, { Address } from '../Profile';

jest.mock('../../../../utils/storage.js', () => ({
  getUserData: () => ({ supplier: {} }),
}));

useSelector.mockImplementation(fn => {
  fn({ form : { profile: {} } });
  return ({ isLoading: false });
});

describe('src/components/forms/Profile', () => {
  test('render', () => {
    const shallow = new ShallowRenderer();
    const tree = shallow.render(<Profile />);
    Profile.defaultProps.handleSubmit();
    Profile.defaultProps.change();
    expect(tree).toMatchSnapshot();
  });

  test('simulate action', () => {
    const setSrc = jest.fn();
    const history = { push: jest.fn() };
    useState.mockImplementationOnce(v => [v, setSrc]);
    useHistory.mockImplementationOnce(() => history);
    const result = Profile(Profile.defaultProps);
    result.props.children[0].props.children[1].props.onPreview('test');
    expect(setSrc).toHaveBeenCalledWith('test');
    expect(result.props.children[1].props.children[6].props.children[1].props.normalize('123a')).toBe('123');
    result.props.children[2].props.children[0].props.onClick();
    expect(history.push).toHaveBeenCalled();
  });

  test('Address', () => {
    const shallow = new ShallowRenderer();
    const tree = shallow.render(<Address />);
    Address.defaultProps.change();
    expect(tree).toMatchSnapshot();
  });

  test('Address simulate action', () => {
    const setOpen = jest.fn();
    const change = jest.fn();
    useState.mockImplementationOnce(() => [false, setOpen]);
    const result = Address({ change });
    result.props.children[0].props.children[1].props.onClick();
    expect(setOpen).toHaveBeenCalledWith(true);
    result.props.children[1].props.onClose();
    expect(setOpen).toHaveBeenCalledWith(false);
    result.props.children[1].props.children[1].props.onPreview('test');
    expect(change).toHaveBeenCalledWith('full_address', 'test');
    result.props.children[1].props.children[1].props.onSubmit({ 'key': 'value' });
    expect(change).toHaveBeenCalledWith('key', 'value');
    expect(setOpen).toHaveBeenCalledWith(false);
  });
});
