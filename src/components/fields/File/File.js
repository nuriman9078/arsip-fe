import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styles from './styles.scoped.css';

export default function File(props) {
  const { className, helper, input, inputProps, maxFile, meta, onPreview, src } = props;
  const { dirty, error, touched } = meta;
  const [fileName, setFileName] = useState(!inputProps.multiple? inputProps.path : '');
  const [fileMultiple, setFileMultiple] = useState(src.map(i => i.url));
  const classes = [
    styles.root,
    fileName && styles['not-empty'],
    className
  ].filter(Boolean).join(' ');
  const iconColor = fileName ? 'white' : 'green';

  const readFiles = (files) => {
    if (inputProps.multiple) {
      let images = [];
      Object.keys(files).forEach(i => {
        const file = files[i];
        const reader = new FileReader();
        const payload = [...input.value, ...Object.values(files)];

        reader.onload = (e) => {
          images[i] = e.target.result;
          input.value.length ? setFileMultiple([...fileMultiple, ...images].slice(0, 5))
            : setFileMultiple([...images].slice(0, 5));
          input.onChange(payload.slice(0, 5));
        };
        reader.readAsDataURL(file);
      });
    } else {
      const reader = new FileReader();
      reader.onload = (e) => {
        const image = e.target.result;
        onPreview(image);
        setFileName(files[0].name);
        input.onChange(files[0]);
      };
      reader.readAsDataURL(files[0]);
    }
  };

  const onChange = (e) => readFiles(e.target.files);
  const onDrop = (e) => {
    e.preventDefault();
    readFiles(e.dataTransfer.files);
  };
  const onDragOver = (e) => {
    e.preventDefault();
    return false;
  };
  const onRemove = (idx) => () => {
    input.onChange(input.value.filter((e, eIdx) => idx !== eIdx));
    setFileMultiple(fileMultiple.filter((e, eIdx) => idx !== eIdx));
  };

  if (Array.isArray(input.value) && input.value.length) {
    return (
      <div className={styles.multiple}>
        {fileMultiple.map((i, idx) => idx < maxFile ? (
          <figure key={idx}>
            <img alt={`upload-${idx}`} onClick={onPreview(i)} src={i} />
            <img alt="remove" onClick={onRemove(idx)} src="/assets/ic-close-outline.svg" />
          </figure>
        ) : null)}
        {fileMultiple.length < maxFile && (
          <label>
            <input onChange={onChange} type="file" {...inputProps} />
            <img alt="add" src="/assets/ic-add-filled.svg" />
          </label>
        )}
        {fileMultiple.length >= maxFile && !error && <p>File sudah berjumlah {maxFile}</p>}
        {!!error && (dirty || touched) && <small>{error}</small>}
      </div>
    );
  }

  return (
    <label className={classes} onDragOver={onDragOver} onDrop={onDrop}>
      <input onChange={onChange} type="file" {...inputProps} />
      <p>
        {inputProps.label} <br />
        <span>{fileName || helper}</span>
      </p>
      <figure>
        {fileName ? <figcaption>Re-upload</figcaption> :
          <figcaption>Drag and Drop your Files or <span>Browse</span></figcaption>}
        <img alt="upload" src={`/assets/ic-upload-${iconColor}.svg`} />
      </figure>
      {!!error && (dirty || touched) && <small>{error}</small>}
    </label>
  );
}

File.defaultProps = {
  className: '',
  helper: '',
  input: {},
  inputProps: {},
  maxFile: Infinity,
  meta: {},
  onPreview: () => { },
  src: []
};

File.propTypes = {
  className: PropTypes.string,
  helper: PropTypes.string,
  input: PropTypes.object,
  inputProps: PropTypes.object,
  maxFile: PropTypes.number,
  meta: PropTypes.object,
  onPreview: PropTypes.func,
  src: PropTypes.array
};
