import React, { useState } from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';
import File from '../File';

describe('src/components/fields/File', () => {
  test('render', () => {
    const shallow = new ShallowRenderer();
    const tree = shallow.render(<File />);
    File.defaultProps.onPreview();
    expect(tree).toMatchSnapshot();
  });

  test('onChange and onDrop single file', () => {
    const file = {
      size: 1000,
      type: '.pdf',
      name: 'test.pdf'
    };
    delete global.FileReader;
    const readAsDataURL = jest.fn();
    const reader = { readAsDataURL };
    global.FileReader = () => reader;

    const setFileName = jest.fn();
    const preventDefault = jest.fn();
    const event = {
      preventDefault,
      dataTransfer: { files: [file], },
      target: { files: [file], },
    };
    const eventOnload = { target: { result: test } };

    useState.mockImplementationOnce((v) => [v, setFileName]);
    const result1 = File({ ...File.defaultProps, input: { onChange: jest.fn() } });
    result1.props.onDrop(event);
    result1.props.children[0].props.onChange(event);
    reader.onload(eventOnload);
    expect(setFileName).toHaveBeenCalledWith(file.name);
    expect(readAsDataURL).toHaveBeenCalledWith(file);
    expect(preventDefault).toHaveBeenCalled();

    result1.props.onDragOver(event);
    expect(preventDefault).toHaveBeenCalled();
  });

  test('onChange and onRemove multiple file', () => {
    const files = [{ size: 1000, type: '.pdf', name: 'test.pdf' }];
    const file = { size: 1000, type: '.pdf', name: 'test2.pdf' };
    const src = ['test1234'];
    delete global.FileReader;
    const readAsDataURL = jest.fn();
    const reader = { readAsDataURL };
    global.FileReader = () => reader;

    const event = {
      dataTransfer: { files: [file]},
      target: { files: [file]},
    };
    const onLoadEvent = { target: { result: 'test' } };

    const result1 = File({ ...File.defaultProps, input: { onChange: jest.fn(), value: files },
      inputProps: { multiple: true }, src });
    result1.props.children[1].props.children[0].props.onChange(event);
    reader.onload(onLoadEvent);
    expect(readAsDataURL).toHaveBeenCalledWith(file);
    result1.props.children[0][0].props.children[1].props.onClick();
  });

  test('error', () => {
    const meta = { active: true, error: 'err', dirty: false, touched: true };

    const result = File({ ...File.defaultProps, meta });
    expect(result.props.children[3].props.children).toBe('err');
  });
});
