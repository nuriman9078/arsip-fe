import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';
import TextArea from '../TextArea';

describe('src/components/fields/TextArea', () => {
  test('render', () => {
    const shallow = new ShallowRenderer();
    const tree = shallow.render(<TextArea />);
    expect(tree).toMatchSnapshot();
  });

  test('error', () => {
    const result1 = TextArea({
      ...TextArea.defaultProps,
      meta: { error: 'err', dirty: true, touched: true }
    });
    expect(result1.props.children[2].props.children).toBe('err');

    const res2 = TextArea({
      ...TextArea.defaultProps,
      meta: { error: 'err', dirty: false, touched: true }
    });
    expect(res2.props.children[2].props.children).toBe('err');
  });

  test('required', () => {
    const textAreaProps = { required: true };

    const result = TextArea({ ...TextArea.defaultProps, textAreaProps });
    expect(result.props.children[0].props.children[1].props.children).toBe('*');
  });
});
