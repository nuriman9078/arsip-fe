import React from 'react';
import PropTypes from 'prop-types';
import { autoPx } from '../../utils/unit';

export default function Expand({ fill, show }) {
  const s24 = autoPx(24);
  const dMore = `M16.5938 8.57812L18 9.98438L12 15.9844L6 9.98438L7.40625 8.57812L12 13.1719L16.5938
    8.57812Z`;
  const dLess = `M12 8.01562L18 14.0156L16.5938 15.4219L12 10.8281L7.40625 15.4219L6 14.0156L12
    8.01562Z`;

  return (
    <svg fill={fill} height={s24} viewBox="0 0 24 24" width={s24} xmlns="http://www.w3.org/2000/svg">
      <path d={show ? dLess : dMore} />
    </svg>
  );
}

Expand.defaultProps = {
  fill: '#424242',
  show: false
};

Expand.propTypes = {
  fill: PropTypes.string,
  show: PropTypes.bool
};
