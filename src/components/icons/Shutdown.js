import React from 'react';
import { autoPx } from '../../utils/unit';

export default function Shutdown() {
  const s24 = autoPx(24);

  return (
    <svg fill="#424242" height={s24} viewBox="0 0 24 24" width={s24} xmlns="http://www.w3.org/2000/svg">
      <path d={`M11.25 3V12H12.75V3H11.25ZM9 3.516C5.5095 4.7535 3 8.091 3 12C3 16.9628 7.03725 21
        12 21C16.9628 21 21 16.9628 21 12C21 8.09025 18.4905 4.7535 15 3.51525V5.133C17.6445 6.2925
        19.5 8.9325 19.5 12C19.5 16.1355 16.1355 19.5 12 19.5C7.8645 19.5 4.5 16.1355 4.5 12C4.5
        8.9325 6.3555 6.2925 9 5.133V3.51525V3.516Z`} />
    </svg>
  );
}
