import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory, useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';
// import Footer from '../../elements/Footer';
// import IconButton from '../../elements/IconButton';
// import Popper from '../../elements/Popper';
// import PopperNotification from '../../elements/PopperNotification';
// import Snackbar from '../../elements/Snackbar';
// import IconCart from '../../icons/Cart';
import IconExpand from '../../icons/Expand';
import IconDashboard from '../../elements/IconDashboard';
import IconPemesanan from '../../elements/IconPemesanan';
// import IconProfile from '../../icons/Profile';
import IconNego from '../../icons/Nego';
import IconSetting from '../../icons/Setting';
// import ShutdownIcon from '../../icons/Shutdown';
import usePopper from '../../../hooks/usePopper';
// import { clearStorages, getUserData } from '../../../utils/storage';
import styles from './styles.scoped.css';

const navs = [
  { icon: IconDashboard, name: 'Dashboard', to: '/' },
  { icon: IconPemesanan, name: 'Add Arsip', to: '/addarsip' },
  {
    icon: IconSetting, name: 'Settings', children: [
      { name: 'Profile', to: '/profile' }
    ],
  },
  { icon: IconNego, name: 'Logout', to: '/login' },
];

export default function PageBase(props) {
  const { children } = props;
  // const { supplier } = getUserData();
  const { pathname } = useLocation();
  const [openPopper, setOpenPopper] = useState(false);
  const pageName = pathname.split('/')[1].toLowerCase();
  const popperProfile = usePopper();

  useEffect(() => {
    const app = document.getElementById('app');
    app.className = 'pagebase';
  }, []);

  // const requiredDoc = [supplier.store_address, supplier.bank_name,
  //   supplier.bank_account_number, supplier.ktp_url, supplier.nib_url,
  //   supplier.npwp_url, supplier.siup_url];

  // const isAccess = (requiredDoc.filter(Boolean).length === requiredDoc.length) || ['profile', 'faq'].includes(pageName);

  // const documentIncomplete = (
  //   <section>
  //     <Snackbar className={styles.error} show>
  //       <p>
  //         Anda Belum bisa mengakses halaman ini karena anda belum melengkapi
  //         informasi dan mengunggah dokumen ini:  <br />
  //         <br />
  //         a. Alamat Perusahaan <br />
  //         b. Nama Bank dan Nomor Rekening <br />
  //         c. KTP <br />
  //         d. NIB <br />
  //         e. SIUP <br />
  //         f. NPWP <br />
  //         <br />
  //         Silahkan ke halaman Profile yang bisa di klik di <Link to="/profile">LINK </Link>ini untuk melengkapi dokumen tersebut.
  //       </p>
  //     </Snackbar>
  //   </section>
  // );
  // const noAuthRoutes = ['/login'];
  // const noAuth = noAuthRoutes.some(r => pathname.match(r));

  // if (noAuth) {
  //   return children;
  // }
  const noAuthRoutes = ['/login', '/register'];
  const noAuth = noAuthRoutes.some(r => pathname.match(r));
  if (noAuth) {
    return children;
  }
  return (
    <>
      <header className={styles.header}>
        <h5>Karyawan</h5>
        <img alt="Agree Market" src="/assets/logo.svg" />
        {/* <Notification /> */}
        {/* <button
          className={styles['btn-profile']}
          onClick={() => setOpenPopper(!openPopper)}
          ref={popperProfile.buttonRef}>
          <span></span>
          <label></label>
          <IconExpand show={openPopper} />
        </button>
        <Popper
          className={styles['popper-profile']}
          onClose={() => setOpenPopper(false)}
          open={openPopper}
          {...popperProfile}>
          <ProfileDropdown />
        </Popper> */}
      </header>
      <aside className={styles.aside}>
        <figure>
          <img alt="profile" src="/assets/ic-avatar.svg" />
          <figcaption>
            <h5>{"Logee Web"}</h5>
            <small>{"nimpuno@yopmail.com"}</small>
          </figcaption>
        </figure>
        <nav>
          {navs.map((n, idx) => n.children ? <ExpandSider data={n} index={idx} key={idx} />
            : <NavSider data={n} key={idx} />)}
        </nav>
      </aside>
      <main className={styles.main}>
        {children}
        {/* <Footer /> */}
      </main>
    </>
  );
}

PageBase.defaultProps = {
  children: null
};

PageBase.propTypes = {
  children: PropTypes.node
};

export function ExpandSider({ data, index }) {
  const { pathname } = useLocation();
  const pages = pathname.split('/')[2];

  const checkIdxParent = navs.reduce((acc, { children = [], }, idx) => {
    const idxChildren = children.findIndex(i => i.to === `/${pathname.split('/')[1].toLowerCase()}`);
    const idxChildrenPages = children.findIndex(i => (
      i.to === `/${pathname.split('/')[1].toLowerCase()}/${pathname.split('/')[2]?.toLowerCase()}`
    ));
    const idxChildrenFix = pages ? idxChildrenPages : idxChildren;

    if (idxChildrenFix !== -1) {
      acc.active = [idx, idxChildrenFix];
      acc.open = idx;
    }

    return acc;
  }, { active: [-1, -1], open: -1 });

  const [expand, setExpand] = useState(checkIdxParent);
  const clickExpand = (index) => () => {
    setExpand({ ...expand, open: expand.open === index ? -1 : index });
  };
  const isOpen = expand.open === index;
  const [idxParent, idxChild] = expand.active;
  const isActive = idxParent === index;
  const { length } = data.children;
  const fillIcon = isActive ? '#74B816' : '#424242';

  useEffect(() => setExpand(checkIdxParent), [pathname]);

  return (
    <div className={styles['nav-item']} id={`sider-parent-${index}`}>
      <section className={isActive ? styles.active : ''} onClick={clickExpand(index)}>
        <data.icon fill={fillIcon} />
        {data.name}
        <IconExpand fill={fillIcon} show={isOpen} />
      </section>
      <ul style={{ maxHeight: isOpen ? `${3 * length}rem` : 0 }}>
        {data.children.map((c, cIdx) => (
          <li key={cIdx}>
            <Link className={isActive && idxChild === cIdx ? styles.active : ''} to={c.to}>{c.name}</Link>
          </li>
        ))}
      </ul>
    </div>
  );
}

ExpandSider.defaultProps = {
  data: {},
  index: null,
};

ExpandSider.propTypes = {
  data: PropTypes.object,
  index: PropTypes.number,
};

export function NavSider({ data }) {
  const { pathname } = useLocation();
  const isActive = data.to === `/${pathname.split('/')[1].toLowerCase()}`;

  return (
    <Link className={isActive ? styles.active : ''} to={data.to}>
      <data.icon fill={isActive ? '#74B816' : '#424242'} />
      {data.name}
    </Link>
  );
}

NavSider.defaultProps = {
  data: {},
};

NavSider.propTypes = {
  data: PropTypes.object,
};

// export function Notification() {
//   const { count, fetchNotification } = useSelector(s => s.notification);
//   const [activeNav, setActiveNav] = useState(0);
//   const [openNotification, setOpenNotification] = useState(false);
//   const popperNotification = usePopper();
//   const dispatch = useDispatch();

//   const onClickIcon = () => {
//     setOpenNotification(!openNotification);
//   };

//   useEffect(() => {
//     openNotification && dispatch(fetchNotification(activeNav));
//   }, [openNotification, activeNav]);

//   return (
//     <>
//       <figure onClick={onClickIcon} ref={popperNotification.buttonRef}>
//         <IconButton icon="notification" size={32} />
//         {!!count && <span>{count < 10 ? count : '9+'}</span>}
//       </figure>
//       <PopperNotification
//         activeNav={activeNav}
//         openNotification={openNotification}
//         popperNotification={popperNotification}
//         setActiveNav={setActiveNav}
//         setOpenNotification={setOpenNotification}
//       />
//     </>
//   );
// }

// export function ProfileDropdown({ supplier }) {
//   const history = useHistory();
//   const { store_name } = supplier;
//   const handleOnClick = () => {
//     location.href = '/';
//     clearStorages();
//   };
//   const data = [{ icon: <IconProfile />, label: 'profil', to: '/profile' }];

//   return (
//     <div className={styles['profile-dropdown']}>
//       <section>
//         <h5>{store_name?.[0]}</h5>
//         <small>{store_name}</small>
//       </section>
//       <section>
//         {data.map((item, id) => (
//           <div className={styles['profile-item']} key={id} onClick={() => history.push(item.to)}>
//             {item.icon}
//             <label>{item.label}</label>
//           </div>
//         ))}
//         <div className={styles['profile-item']} onClick={() => handleOnClick()}>
//           <ShutdownIcon />
//           <label>logout</label>
//         </div>
//       </section>
//     </div>
//   );
// }

// ProfileDropdown.defaultProps = {
//   supplier: {},
// };

// ProfileDropdown.propTypes = {
//   supplier: PropTypes.object,
// };
