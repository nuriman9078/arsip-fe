import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import IconButton from '../IconButton';
import Modal from '../Modal';
import styles from './styles.scoped.css';

export default function Toast(modalProps) {
  const { messageBody, messageHead, redirect, status } = modalProps;
  const [open, setOpen] = useState(false);

  useEffect(() => {
    setOpen(true);
  }, []);

  const closeToast = () => {
    setOpen(false);
    if(redirect){
      location.href = redirect;
    }
  };

  return (
    <Modal className={styles.root} onClose={closeToast} open={open} >
      <div className={status ? styles[status] : styles.error}>
        <IconButton className="modal-icon" icon="close" onClick={closeToast} />
        <p>{messageHead}</p>
        <p>{messageBody}</p>
      </div>
    </Modal>
  );
}

Toast.defaultProps = {
  messageBody: '',
  messageHead: '',
  redirect: ''
};

Toast.propTypes = {
  messageBody: PropTypes.string,
  messageHead: PropTypes.string,
  redirect: PropTypes.string
};
