import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.scoped.css';

export default function Snackbar(props) {
  const { children, className, show, variant } = props;
  const classes = [
    styles.root,
    !show && styles.hidden,
    styles[variant],
    className
  ].join(' ');

  return (
    <div className={classes}>
      <img alt="info" src="/assets/ic-info.svg" />
      {children}
    </div>
  );
}

Snackbar.defaultProps = {
  children: '',
  className: null,
  show: false,
  variant: 'warning',
};

Snackbar.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  show: PropTypes.bool,
  variant: PropTypes.oneOf(['warning', 'error', 'success']),
};
