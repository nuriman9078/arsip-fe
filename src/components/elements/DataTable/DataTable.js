import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.scoped.css';
import Loader from '../../elements/Loader';

export default function DataTable(props) {
  const { className, column, data, emptyChildren, meta, isLoading } = props;
  const classes = [styles.root, className].filter(Boolean).join(' ');

  return (
    <>
      <table className={classes}>
        <thead>
          <tr>
            <th>No</th>
            {column.map((item, idx) => <TableHeader item={item} key={idx} />)}
          </tr>
        </thead>
        <tbody>
          {isLoading &&  <tr><td colSpan={column.length+1}><Loader /></td></tr> }
          { !isLoading && data && data.map((item, idx) => (
            <TableRow
              className={item.className || ''}
              column={column}
              idx={idx}
              item={item}
              key={idx}
              meta={meta}
            />
          ))}
        </tbody>
      </table>
      {
        emptyChildren ?
          ( data && !data[0] ) &&
            emptyChildren :
          ( data && !data[0] ) &&
            <div className={styles.notFound}>
              <img src="../../assets/ic-not-found.svg"/>
            </div>
      }
    </>
  );
}

DataTable.defaultProps = {
  className: '',
  column: [],
  data: [],
  emptyChildren: null,
  isLoading:false,
  meta: {},
};

DataTable.propTypes = {
  className: PropTypes.string,
  column: PropTypes.array,
  data: PropTypes.array,
  emptyChildren: PropTypes.object,
  isLoading:PropTypes.bool,
  meta: PropTypes.object
};

export function TableHeader({ item }) {

  return (
    <th className={styles['table-header']}>{item.heading}</th>
  );
}

TableHeader.defaultProps = {
  item: {},
};

TableHeader.propTypes = {
  item: PropTypes.object,
};

export function TableRow({ className, column, idx, item,meta }) {
  const { page } = meta;
  const size = 10;
  let b = idx+1;
  if ( page > 1 ) {
    b = ( idx+1 )+(( page - 1 ) * size );
  }
  return (
    <tr className={className}>
      <td>{b < 10 ? `0${b}` : b}</td>
      {column.map((cItem, cIdx) => {
        const { value } = cItem;
        const newValue = typeof value === 'function' ? value(item, idx) : item[value];

        return (
          <td key={cIdx}>
            {newValue || '-'}
          </td>
        );
      })}
    </tr>
  );
}

TableRow.defaultProps = {
  className: '',
  column: [],
  idx:0,
  item: {},
  meta:{}
};

TableRow.propTypes = {
  className: PropTypes.string,
  column: PropTypes.array,
  idx: PropTypes.number,
  item: PropTypes.object,
  meta: PropTypes.object,
};
