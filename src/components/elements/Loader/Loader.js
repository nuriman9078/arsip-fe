import React from 'react';
import styles from './styles.scoped.css';
import PropTypes from 'prop-types';

export default function Loader(props) {
  const root = [props.className, styles.root].join(' ');
  return (
    <div {...props} className={root}>
      <div className="loading"/>
    </div>
  );
}

Loader.defaultProps = {
  className: ''
};

Loader.propTypes = {
  className: PropTypes.string
};
