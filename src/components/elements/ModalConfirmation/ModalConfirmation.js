import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Button';
import IconButton from '../IconButton';
import Modal from '../Modal';
import styles from './styles.scoped.css';

export default function ModalConfirmation(modalProps) {
  const {
    children,
    disabled,
    isLoading,
    messageBody,
    messageHead,
    send,
    closeButton,
    submitButton,
    ...props
  } = modalProps;
  return (
    <Modal className={styles.root} {...props}>
      <div>
        <IconButton className="modal-icon" icon="close" onClick={props.onClose} />
        <p>{messageHead}</p>
        <p>{messageBody}</p>
        {children}
      </div>
      <footer>
        <Button onClick={props.onClose}>{closeButton || 'Batal'}</Button>
        <Button disabled={disabled} isLoading={isLoading} onClick={send}>{submitButton || 'Simpan'}</Button>
      </footer>
    </Modal>
  );
}

ModalConfirmation.defaultProps = {
  children: null,
  closeButton: '',
  disabled: false,
  isLoading: false,
  messageBody: '',
  messageHead: '',
  onClose: () => { },
  send: () => { },
  submitButton: ''
};

ModalConfirmation.propTypes = {
  children: PropTypes.node,
  closeButton: PropTypes.string,
  disabled: PropTypes.bool,
  isLoading: PropTypes.bool,
  messageBody: PropTypes.string,
  messageHead: PropTypes.string,
  onClose: PropTypes.func,
  send: PropTypes.func,
  submitButton: PropTypes.string
};
