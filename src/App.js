import React from 'react';
import PropTypes from 'prop-types';
import { hot } from 'react-hot-loader/root';
import { Provider } from 'react-redux';
import { Router } from 'react-router';
import { Switch, Route } from 'react-router-dom';
import AppContextProvider from './contexts';
import PageBase from './components/layouts/PageBase';
import pages from './pages';

// const afterLoginRoutes = [
//   { c: pages.Cart, key: 'Cart', path: '/cart', },
//   { c: pages.PaymentDetail, key: 'PaymentDetail', path: '/payment', },
//   { c: pages.Profile, key: 'Profile', path: '/profile', },
// ];

const App = ({ history, store }) => {
  // if (getToken() && !checkExpireTime()) {
  //   clearStorages();
  //   location.reload();
  //   return null;
  // }
  return (
    <Provider store={store}>
      <Router history={history}>
        <AppContextProvider>
          <PageBase>
            <Switch>
              <Route component={pages.Dashboard} exact path="/" />
              <Route component={pages.Login} exact path="/login" />
              <Route component={pages.Register} exact path="/register" />
              <Route component={pages.Profile} exact path="/profile" />
              <Route component={pages.AddArsip} exact path="/addarsip" />
              <Route component={pages.Error404} />
            </Switch>
          </PageBase>
        </AppContextProvider>
      </Router>
    </Provider>
  );
};

export default hot(App);

App.propTypes = {
  history: PropTypes.object.isRequired,
  store: PropTypes.object.isRequired
};
