import React, { createContext, useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';
import { getCartData } from '../utils/storage';

export const AppContext = createContext({});

export default function AppContextProvider({ children }) {
  const [modal, setModal] = useState(null);
  const [onCloseModal, setOnCloseModal] = useState(() => () => { });
  const [cart, setCart] = useState(0);
  const { pathname } = useLocation();
  const value = {
    cart,
    setModal,
    setOnCloseModal
  };

  useEffect(() => {
    setCart(getCartData().length);
  }, [getCartData()]);

  const closeModal = e => {
    if (e.target === e.currentTarget) {
      onCloseModal();
    }
  };

  useEffect(() => {
    const body = document.getElementsByTagName('body')[0];
    modal ? body.classList.add('modal-open') : body.classList.remove('modal-open');
  }, [!!modal]);

  useEffect(() => {
    document.getElementsByTagName('html')[0].scrollTo(0, 0);
    setModal(null);
  }, [pathname]);

  return (
    <AppContext.Provider value={value}>
      {children}
      <div className="modal" onClick={closeModal}>{modal}</div>
    </AppContext.Provider>
  );
}

AppContextProvider.defaultProps = {
  children: null,
};

AppContextProvider.propTypes = {
  children: PropTypes.node,
};
