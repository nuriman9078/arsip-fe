import React from 'react';
import Webp from '../../components/elements/Webp';
import RegisterForm from '../../components/forms/Register';
import Button from '../../components/elements/Button';
import styles from './styles.scoped.css';

export default function Register() {
  return (
    <main className={styles.root}>
      <section className={styles.body}>
        <RegisterPage />
      </section>
    </main>
  );
}

export function RegisterPage() {
  return (
    <>
      <figure>
        <figcaption>
          <h4>Daftar Akun Anda</h4>
          <p>Kami Memberikan Kemudahan Untuk Memenuhi Kebutuhan Penjualan Perusahaan Anda</p>
        </figcaption>
        <Webp alt="Daftarkan Akun Anda" name="arsip" />
      </figure>
      <div>
        <RegisterForm />
        <article>
          <label>Sudah punya akun Arsip?</label>
          <Button>Masuk</Button>
        </article>
      </div>
    </>
  );
}
