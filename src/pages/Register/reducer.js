import { REGISTERED, FAILED, LOADING } from './constants';

const initialState = {
  isLoading: false,
  data: {},
  message: '',
};

export default function reducer(state = initialState, action = {}) {
  const { type, isLoading, data, message } = action;

  switch (type) {
    case FAILED:
      return {
        ...state,
        isLoading: false,
        message,
      };
    case REGISTERED:
      return {
        ...state,
        isLoading: false,
        data,
      };
    case LOADING:
      return {
        ...state,
        isLoading: isLoading,
      };
    default:
      return state;
  }
}
