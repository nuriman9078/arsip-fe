// export { default } from './AddArsip';
import { reduxForm } from 'redux-form';
import Component from './AddArsip';
import validate from './validate';

export default reduxForm({
  form: 'addarsip',
  validate
})(Component);
