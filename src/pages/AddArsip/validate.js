import { isMobilePhone } from '../../utils/validation';

export default function validate(values) {
  const { store_name, name, bank_name, position, mobile_number, bank_account_number,
    full_address, logo_path } = values;
  const required = 'Harus diisi!';

  return {
    store_name: !store_name ? required : '',
    name: !name ? required : '',
    bank_name: !bank_name ? required : '',
    position: !position ? required : '',
    bank_account_number: !bank_account_number ? required : '',
    full_address: !full_address ? required : '',
    logo_path: !logo_path ? required : '',
    mobile_number: !isMobilePhone(mobile_number) ? 'Mohon memasukkan no. handphone yang benar' : '',
  };
}
