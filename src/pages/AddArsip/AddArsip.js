import React from 'react';
// import { useHistory } from 'react-router-dom';
// import PropTypes from 'prop-types';
// import { useSelector } from 'react-redux';
import { Field } from 'redux-form';
import Button from '../../components/elements/Button';
// import Modal from '../../components/elements/Modal';
import TextField from '../../components/fields/Text';
import TextAreaField from '../../components/fields/TextArea';
// import FileField from '../../components/fields/File';
// import { getUserData } from '../../../utils/storage';
// import AddressForm from '../../components/forms/Address';
import styles from './styles.scoped.css';

export default function AddArsip(props) {

  const textProps = [
    { label: 'Title Arsip', placeholder: 'Isikan nama arsip', required: true },
    { label: 'Description', placeholder: 'Isikan Description arsip', required: true },
  ];

  return (
    <section className={styles.root}>
      <header>
        <h4>Tambah Arsip</h4>
      </header>
      <article>
        <form>
          <figure>
            <img alt="broken-image" src={"/assets/img-empty.svg"} />
          </figure>
          <form>
            <Field component={TextField} inputProps={textProps[0]} name="store_name" />
            <Field component={TextAreaField} name="store_description" textAreaProps={textProps[1]} />
          </form>
          <footer>
            <Button variant="ghost">Batal</Button>
            <Button type="submit">Simpan</Button>
          </footer>
        </form>
      </article>
    </section>
  )
}