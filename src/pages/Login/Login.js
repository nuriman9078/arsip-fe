import React from 'react';
import { useHistory} from 'react-router-dom';
import LoginForm from '../../components/forms/Login';
import styles from './styles.scoped.css';

export default function Login() {
  const history = useHistory();
  const login = (values) => {
    !!values.email && (location.href = '/');
  }
  return (
    <main className={styles.root}>
      <figure>
        <img alt="Logee" src="/assets/arsip.png" />
      </figure>
      <section>
        <h3>
          Selamat Datang di
          <br /><span>Arsip</span>
        </h3>
        <p>Solusi untuk mengoptimalkan segala kebutuhan arsip
          perusahaan di seluruh Indonesia.</p>
        <LoginForm onSubmit={login}/>
      </section>
    </main>
  );
}
