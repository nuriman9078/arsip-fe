import React from 'react';
import LazyFallback from '../components/elements/LazyFallback';

const Suspensed = (Element) => function suspense(props) {
  return (
    <React.Suspense fallback={<LazyFallback />}>
      <Element {...props} />
    </React.Suspense>
  );
};

export default {
  Login: Suspensed(React.lazy(() => import('./Login'))),
  Register: Suspensed(React.lazy(() => import('./Register'))),
  Dashboard: Suspensed(React.lazy(() => import('./Dashboard'))),
  Profile: Suspensed(React.lazy(() => import('./Profile'))),
  AddArsip: Suspensed(React.lazy(() => import('./AddArsip'))),
  Error404: Suspensed(React.lazy(() => import('./Error404')))
};
