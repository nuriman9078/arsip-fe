import React, { useCallback, useEffect, useState } from 'react';
import moment from 'moment';
// import { useDispatch, useSelector } from 'react-redux';
// import { useLocation, useHistory } from 'react-router-dom';
import DataTable from '../../components/elements/DataTable';
import { fetchData, hapusUser } from './actions';
import queryString from 'querystring';
import style from './styles.scoped.css';
import Pagination from '../../components/elements/Pagination';
import ModalConfirmation from '../../components/elements/ModalConfirmation';
import Toast from '../../components/elements/Toast';
import Search from '../../components/elements/Search';
import { formatDate } from '../../utils/format';

export default function Dashboard(){
  // const dispatch = useDispatch();
  // const history = useHistory();
  // const location = useLocation();
  // const { push, parse } = useQuery();
  const parsedQuery = queryString.parse(location.search.replace('?',''));
  const { page } = parsedQuery;
  const [errorSearch, setErrorSearch] = useState('');
  const [keywordSearch, setKeywordSearch] = useState('');
  const [filterState, setFilter ] = useState({
    startDate:'',
    endDate:'',
    roles:'',
    companyId: ''
  });

  const [redirectLinkDelete, setRedirectLinkDelete] = useState('');
  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const [messageHead] = useState('Hapus User');
  const [messageBody, setMessageBody] = useState({
    userId:'',
    message:''
  });
  const [toastMessageHead] = useState({
    success: 'Berhasil !',
    failed: 'Gagal !'
  });
  const [toastMessageBody] = useState({
    success: 'Data user berhasil dihapus.',
    failed: 'Data user gagal dihapus'
  });

  const _handleOpenDeleteModal = useCallback((item)=>{
    setMessageBody({ ...messageBody,
      userId:item.userId,
      message:(<div className={style.messageBody}>
        Apakah Anda yakin menghapus user <b> {item.name || ''}</b> ?
      </div>)
    });
    setOpenDeleteModal(true);
  }, [openDeleteModal, messageBody]);

  const appTypes = {
    truck: 'transport-truck',
    cargo: 'transport-cargo',
    driver: 'transport-driver',
    portal: 'port-npct1'
  };

  const [openToast, setOpenToast] = useState({
    isOpen: false,
    isSuccess: false
  });

  const getUserApps = key => {
    if(!key){
      return key;
    } else if(key === appTypes.cargo){
      return 'Cargo';
    } else if(key === appTypes.truck){
      return 'Trucker';
    } else if(key === appTypes.driver){
      return 'Driver';
    } else if(key === appTypes.portal) {
      return 'Portal';
    } else {
      return '';
    }
  };

  const objectToQuerystring = Object.keys(parsedQuery)
    .map(key => {
      let selectedDate;
      if(key === 'startDate'){
        selectedDate = moment(`${parsedQuery[key]} 00:00:00`).format('YYYY-MM-DDTHH:mm:ss');
        return `${key}=${selectedDate}`;
      } else if (key === 'endDate'){
        selectedDate = moment(`${parsedQuery[key]} 23:59:59`).format('YYYY-MM-DDTHH:mm:ss');
        return `${key}=${selectedDate}`;
      }
      return `${key}=${parsedQuery[key]}`;
    })
    .join('&');

  const inputProps = {
    placeholder : 'Cari Nama Arsip',
    maxLength: 50
  };

  const resetFilter = () => {
    setFilter({ ...filterState, startDate:'', endDate:'', roles:'', companyId: '' });
    history.push('?');
  };

  const submitFilter = () => {
    delete parse.keyword;
    delete parse.page;
    Object.keys(parse).forEach(key => {
      if(!filterState[key]){
        delete parse[key];
      }
    });
    push(filterState);
  };

  const submitSearch = () => {
    if(keywordSearch && keywordSearch.length < 3){
      setErrorSearch('Minimum 3 karakter');
    } else {
      const filter = {};
      if(keywordSearch){
        filter.keyword = keywordSearch;
      } else {
        delete parse.keyword;
      }
      delete parse.page;
      delete parse.startDate;
      delete parse.endDate;
      delete parse.roles;
      delete parse.companyId;
      setFilter({ ...filterState, startDate:'', endDate:'', roles:'', companyId: '' });
      push(filter);
    }
  };

  const status = (i) => i ? <p className={style.active}>ACTIVE</p> :
    <p className={style.inactive}>INACTIVE</p>;

  const renderAction = (item) => (
    <div className={style.boxAction}>
      <img alt="Delete" onClick={() => _handleOpenDeleteModal(item)} src={'../../../assets/ic-delete.svg'}/>
    </div>
  );

  const date = (i) => ( <a className={style.date}> { formatDate(i,'LL')} </a> );

  const column = [
    { heading: 'Name', value: 'name' },
    { heading: 'Description', value: 'phone' },
    { heading: 'Admin', value: ({ createdAt }) => date(`${formatDate(createdAt)}`) },
    { heading: 'Download Date', value: ({ apps }) => getUserApps(apps) || '' },
    { heading: 'Hour', value: 'companyName' },
    { heading: '', value: (item) => renderAction(item) }
  ];

  const deleteUser = async () => {
    try {
      const routes = await dispatch(hapusUser(messageBody.userId, location.search));
      setOpenDeleteModal(false);
      setOpenToast({
        isOpen: true,
        isSuccess: true
      });
      setRedirectLinkDelete(routes);
    } catch (error) {
      setOpenDeleteModal(false);
      setOpenToast({
        isOpen: true,
        isSuccess: false
      });
    }
  };

  const renderToast = () => {
    if(openToast.isSuccess){
      return (
        <Toast
          messageBody={toastMessageBody.success}
          messageHead={toastMessageHead.success}
          redirect={redirectLinkDelete}
          status="success"
        />
      );
    } else {
      return (
        <Toast
          messageHead={toastMessageHead.failed}
          status={false}
        />
      );
    }
  };

  return(
    <div className={style.root}>
      <section >
        <h4>Arsip</h4>
        <section>
          <Search
            handleSubmit={submitSearch}
            inputProps={inputProps}
            keyword={keywordSearch}
          />
          <small>{errorSearch}</small>
        </section>
      </section>
      <DataTable className={style.tableRoot} column={column}/>
      <Pagination location={location} />
      {openDeleteModal && (
        <ModalConfirmation
          closeButton="Batal"
          messageBody={messageBody.message}
          messageHead={messageHead}
          onClose={() => setOpenDeleteModal(false)}
          open={openDeleteModal}
          send={deleteUser}
          submitButton="Hapus"
        />
      )}
      {openToast.isOpen && renderToast()}
    </div>
  );
}
