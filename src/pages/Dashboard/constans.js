export const DATA_FETCHED = 'ListUser/data-fetched';
export const LOADING = 'ListUser/loading';
export const FAILED = 'ListUser/failed';
export const DATA_FETCHED_SELECT = 'ListUser/data-fetched-select';
