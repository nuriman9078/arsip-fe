import { DATA_FETCHED, FAILED, LOADING, DATA_FETCHED_SELECT } from './constans';

const initialState = {
  listUser: null,
  listDropCompany: [],
  meta: {},
  isLoading: {
    deleteUser: false,
    listUser: false
  },
};

export default function reducer(state = initialState, action = {}) {
  const { type, data, meta, isLoading, key, message } = action;
  switch (type) {
    case DATA_FETCHED:
      return {
        ...state,
        isLoading: { ...state.isLoading, [key]:false },
        [key]: data,
        meta
      };
    case DATA_FETCHED_SELECT:
      return {
        ...state,
        isLoading: { ...state.isLoading, [key]:false },
        [key] : data,
      };
    case LOADING:
      return {
        ...state,
        isLoading: { ...state.isLoading, [key] : isLoading }
      };
    case FAILED:
      return {
        ...state,
        isLoading: { ...state.isLoading, [key] : isLoading },
        message,
      };
    default:
      return state;
  }
}
