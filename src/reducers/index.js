import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import login from '../pages/Login/reducer';
import register from '../pages/Register/reducer';
import dashboard from '../pages/Dashboard/reducer';
import profile from '../pages/Profile/reducer';

const rootReducer = combineReducers({
  form: formReducer,
  dashboard,
  login,
  register,
  profile,
});

export default rootReducer;
