export const updateSyncErrors = (form, syncErrors) => ({
  type: '@@redux-form/UPDATE_SYNC_ERRORS',
  meta: { form },
  payload: { syncErrors },
});
