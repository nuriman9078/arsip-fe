const USER_DATA_STORAGE = 'agree_market_user_data';
const TOKEN_STORAGE = 'agree_market_user_access_token';
const EXPIRE_TIME_STORAGE = 'agree_market_user_token_expiry_time';
const TOKEN_TYPE_STORAGE = 'agree_market_user_token_type';
const CART_DATA_STORAGE = 'agree_market_user_cart_data';

export function setToken(value) {
  localStorage.setItem(TOKEN_STORAGE, value);
}

export function getToken() {
  return localStorage.getItem(TOKEN_STORAGE);
}

export function setTokenType(value) {
  localStorage.setItem(TOKEN_TYPE_STORAGE, value);
}

export function getTokenType(value) {
  return localStorage.getItem(TOKEN_TYPE_STORAGE, value);
}

export function clearStorages() {
  localStorage.removeItem(TOKEN_STORAGE);
  localStorage.removeItem(EXPIRE_TIME_STORAGE);
  localStorage.removeItem(USER_DATA_STORAGE);
  localStorage.removeItem(TOKEN_TYPE_STORAGE);
  localStorage.removeItem(CART_DATA_STORAGE);
}

export function setExpireTime(value) {
  localStorage.setItem(EXPIRE_TIME_STORAGE, value);
}

export function checkExpireTime() {
  const time = new Date().getTime();
  const expire = localStorage.getItem(EXPIRE_TIME_STORAGE) || 0;

  return time > expire;
}

export function setUserData(value) {
  localStorage.setItem(USER_DATA_STORAGE, JSON.stringify(value));
}

export function getUserData() {
  const retval = localStorage.getItem(USER_DATA_STORAGE);

  return JSON.parse(retval) || '';
}

export function getCartData() {
  const retval = localStorage.getItem(CART_DATA_STORAGE);

  return JSON.parse(retval) || [];
}

export function setCartData(product, type = 'set', qty = 0) {
  let cart = JSON.parse(localStorage.getItem(CART_DATA_STORAGE) ?? '[]');
  const idList = cart.map(item => item.id);
  switch (type) {
    case 'add': {
      if (qty > product.stock) {
        qty = product.stock;
      }
      for (const item of cart) {
        if (item.id && item.id === product.id) {
          item.quantity += qty;
          if (item.quantity > product.stock) {
            item.quantity = product.stock;
          }
        }
      }
      if (!idList.includes(product.id)) {
        cart.push({
          ...product,
          quantity: qty,
        });
      }
      break;
    }
    case 'set': {
      for (const item of cart) {
        if (item.id && item.id === product.id) {
          item.quantity = qty;
        }
      }
      break;
    }
    case 'del': {
      cart = cart.filter(item => item.id !== product.id);
      break;
    }
  }
  return localStorage.setItem(CART_DATA_STORAGE, JSON.stringify(cart));
}
