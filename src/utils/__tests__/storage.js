import { set } from 'mockdate';
import * as storage from '../storage';

delete window.localStorage;
window.localStorage = {};

describe('src/utils/storage', () => {
  test('setToken', () => {
    window.localStorage.setItem = jest.fn();
    storage.setToken('tes');
    expect(window.localStorage.setItem).toHaveBeenCalledWith('agree_market_user_access_token', 'tes');
  });

  test('getToken', () => {
    window.localStorage.getItem = jest.fn(() => 'tes');
    expect(storage.getToken()).toBe('tes');
  });

  test('clearStorages', () => {
    window.localStorage.removeItem = jest.fn();
    storage.clearStorages();
    expect(window.localStorage.removeItem).toHaveBeenNthCalledWith(1, 'agree_market_user_access_token');
    expect(window.localStorage.removeItem).toHaveBeenNthCalledWith(2, 'agree_market_user_token_expiry_time');
    expect(window.localStorage.removeItem).toHaveBeenNthCalledWith(3, 'agree_market_user_data');
  });

  test('setExpireTime', () => {
    window.localStorage.setItem = jest.fn();
    storage.setExpireTime(1);
    expect(window.localStorage.setItem).toHaveBeenCalledWith('agree_market_user_token_expiry_time', 1);
  });

  test('checkExpireTime', () => {
    window.localStorage.getItem = jest.fn(() => { });
    set('2020-09-11');
    expect(storage.checkExpireTime()).toBe(true);
    window.localStorage.getItem = jest.fn(() => 100);
    expect(storage.checkExpireTime()).toBe(true);
  });

  test('setUserData', () => {
    window.localStorage.setItem = jest.fn();
    const data = { tes: 'tes' };
    storage.setUserData(data);
    expect(window.localStorage.setItem).toHaveBeenCalledWith('agree_market_user_data', JSON.stringify(data));
  });

  test('getUserData', () => {
    window.localStorage.getItem = jest.fn(() => JSON.stringify(null));
    expect(storage.getUserData()).toBe('');
    window.localStorage.getItem = jest.fn(() => JSON.stringify({ tes: 'tes' }));
    expect(storage.getUserData().tes).toBe('tes');
  });

  test('getCartData', () => {
    window.localStorage.getItem = jest.fn(() => JSON.stringify(null));
    expect(storage.getCartData()).toStrictEqual([]);
    window.localStorage.getItem = jest.fn(() => JSON.stringify({ tes: 'test' }));
    expect(storage.getCartData().tes).toBe('test');
  });
});
