import moment from 'moment';
import queryString from 'querystring';
import { useHistory, useLocation } from 'react-router-dom';

moment.locale('id');

export const thousand = val => (
  Math.round(val).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
);

export const formatDate = (date, format) => date && moment(date).format(format);

export const removeDot = val => (
  val.toString().replace(/\./g, '')
);

export const percentageNumber = val => {
  const string = ((val * 10) * 10).toString();
  if (string.includes('.')) {
    const split = Math.round(Number(`0.${string.split('.')[1]}`) * 10);
    const convert = split.toString();
    return `${string.split('.')[0]}.${convert.charAt(0)}`;
  } else {
    return string;
  }
};

export const toTitleCase = (str) => {
  str.replace(
    /\w\S*/g,
    function (txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    }
  );
};

export const generateTextNotif = (value) => {
  const generateText = {
    text1: '',
    text2: '',
    text3: ''
  };

  const _textSplit = value.split('<span>');
  if (_textSplit.length === 0) {
    generateText.text1 = '';
  } else if (_textSplit.length === 1) {
    generateText.text1 = _textSplit[0];

  } else {
    generateText.text1 = _textSplit[0];
    if (_textSplit[1]) {
      generateText.text2 = _textSplit[1].split('</span>')[0];
      generateText.text3 = _textSplit[1].split('</span>')[1];
    }
  }
  return generateText;
};

export const generateTypeNotif = (value) => {
  const _value = value || '';
  let _typeNotif = '-';
  if (_value.toLowerCase().indexOf('order') >= 0) {
    _typeNotif = 'Transaksi';
  } else if (_value === 'user-created') {
    _typeNotif = 'User Baru';
  }
  return _typeNotif;
};

export function useQuery() {
  const history = useHistory();
  const location = useLocation();
  const parse = queryString.parse(location.search.replace('?', ''));
  const stringify = (params) => queryString.stringify({ ...parse, ...params });

  return { parse, push: (v) => history.push(`?${stringify(v)}`) };
}
